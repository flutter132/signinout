import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:login/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

class HomeScreen extends StatelessWidget {
	const HomeScreen({Key? key}) : super(key: key);

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			body: Column(
				children: [
					Expanded(
						flex: 1,
						child: Padding(
							padding: EdgeInsets.symmetric(horizontal: 0.05),
							child: Column(
								mainAxisAlignment: MainAxisAlignment.spaceEvenly,
								children: [
									Text("Welcome",
											style: TextStyle(
												fontSize: 20,
												fontWeight: FontWeight.w700,
												color: Color(0xFF166FFF),
												fontFamily: "Poppins",
											)),
									Padding(
										padding: const EdgeInsets.symmetric(horizontal: 60.0),
										child: GestureDetector(
											onTap: () {
												Dio().get('http://10.0.2.2:3000/api/logout')
														.then((value) {
													print(value);
													try {
														Navigator.pushReplacement(
															context,
															MaterialPageRoute(
																builder: (context) => WelcomeScreen(),
															),
														);
													} catch (e) {
														toast("ERROR");
													}
												}).onError((error, stackTrace) {
													print(error);
													toast("ERROR");
												});
											},
											child: Container(
												width: MediaQuery.of(context).size.width,
												padding: EdgeInsets.symmetric(vertical: 13),
												alignment: Alignment.center,
												decoration: BoxDecoration(
													borderRadius: BorderRadius.all(Radius.circular(5)),
													border:
													Border.all(color: Color(0xff14279B), width: 2),
												),
												child: Row(
													mainAxisAlignment: MainAxisAlignment.center,
													children: [
														SizedBox(width: 0.26),
														Text("Log out",
																style: TextStyle(
																	fontSize: 17,
																	fontWeight: FontWeight.w500,
																	color: Color(0xFF166FFF),
																	fontFamily: "Poppins",
																)),
														SizedBox(
															width: 5,
														),
														Icon(
															Icons.arrow_forward,
															size: 24,
															color: Color(0xFF166FFF),
														),
														SizedBox(width: 0.05),
													],
												),
											),
										),
									),
								],
							),
						),
					)
				],
			),
		);
	}
}
void toast(String msg){
	Fluttertoast.showToast(
			msg: msg,
			toastLength: Toast.LENGTH_SHORT,
			gravity: ToastGravity.BOTTOM,
			timeInSecForIosWeb: 1,
			backgroundColor: Colors.black54,
			textColor: Colors.white,
			fontSize: 16.0
	);
}