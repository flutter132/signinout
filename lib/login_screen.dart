import 'dart:convert';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:login/home_screen.dart';
import 'package:login/signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginScreen extends StatefulWidget {
	const LoginScreen({Key? key}) : super(key: key);

	@override
	_LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
	String loginEmail="";
	String loginPassword="";
	bool isAuth=false;

	TextEditingController passwordController = new TextEditingController();
	TextEditingController emailController = new TextEditingController();

	Future<List<String>> login() async {
		List<String> info = ["", ""];
		Response response;
		var dio = Dio();
		response = await dio.post('localhost:3000/api/register', data: {'email': loginEmail, 'password': loginPassword});
		print(response);

		Map<String, dynamic> data = json.decode(response.data);
		if (data.containsKey("isAuth") && data["isAuth"] == true){
			info[0] = data["id"];
			info[1] = data["email"];
		}else{
			info[0] = data["message"];
		}
		return info;
	}


	@override
	Widget build(BuildContext context) {
		final height = MediaQuery.of(context).size.height;
		return Scaffold(
				body: Container(
					height: height,
					child: Stack(
						children: <Widget>[
							Positioned(
								child: Container(
											child: ClipPath(
												child: Container(
													decoration: BoxDecoration(
														gradient: LinearGradient(
															begin: Alignment.topCenter,
															end: Alignment.bottomCenter,
															colors: [
																Color(0xffE6E6E6),
																Color(0xff14279B),
															],
														),
													),
												),
										)),
							),
							Container(
								padding: EdgeInsets.symmetric(horizontal: 40),
								child: SingleChildScrollView(
									child: Column(
										crossAxisAlignment: CrossAxisAlignment.center,
										mainAxisAlignment: MainAxisAlignment.center,
										children: <Widget>[
											SizedBox(height: height * .3),
											Column(
												children: <Widget>[
													Container(
														margin: EdgeInsets.symmetric(vertical: 10),
														child: Column(
															crossAxisAlignment: CrossAxisAlignment.start,
															children: <Widget>[
																Text(
																	"Email",
																	style: TextStyle(
																			fontWeight: FontWeight.bold, fontSize: 15),
																),
																SizedBox(
																	height: 10,
																),
																TextField(
																	controller: emailController,
																		obscureText: false,
																		decoration: InputDecoration(
																				border: InputBorder.none,
																				fillColor: Color(0xfff3f3f4),
																				filled: true),
																),
															],
														),
													),
													Container(
														margin: EdgeInsets.symmetric(vertical: 10),
														child: Column(
															crossAxisAlignment: CrossAxisAlignment.start,
															children: <Widget>[
																Text(
																	"Password",
																	style: TextStyle(
																			fontWeight: FontWeight.bold, fontSize: 15),
																),
																SizedBox(
																	height: 10,
																),
																TextField(
																		controller: passwordController,
																		obscureText: true,
																		decoration: InputDecoration(
																				border: InputBorder.none,
																				fillColor: Color(0xfff3f3f4),
																				filled: true),
																),
															],
														),
													)
												],
											),
											SizedBox(height: 20),
											GestureDetector(
												onTap: () {
													if (emailController.text.isEmpty || passwordController.text.isEmpty){
														toast("Email or Passwor is Empty!");
														return;
													}

													Dio().post('http://10.0.2.2:3000/api/login', data: {'email': emailController.text, 'password': passwordController.text})
														.then((value) {
															print(value);
															try{
																Map<String, dynamic> data = json.decode(value.toString());
																if (data.containsKey("isAuth") && data["isAuth"] == true){
																	Navigator.pushReplacement(
																		context,
																		MaterialPageRoute(
																			builder: (context) => HomeScreen(),
																		),
																	);
																}else{
																	toast(data["message"]);
																}
															}catch(e){
																toast("ERROR");
															}
														}).onError((error, stackTrace) {
														print(error);
														toast("ERROR");
													} );
												},
												child: Container(
													width: MediaQuery.of(context).size.width,
													padding: EdgeInsets.symmetric(vertical: 15),
													alignment: Alignment.center,
													decoration: BoxDecoration(
														borderRadius: BorderRadius.all(Radius.circular(5)),
														boxShadow: <BoxShadow>[
															BoxShadow(
																	color: Colors.grey.shade200,
																	offset: Offset(1, 2),
																	blurRadius: 5,
																	spreadRadius: 1)
														],
														gradient: LinearGradient(
															begin: Alignment.centerLeft,
															end: Alignment.centerRight,
															colors: [
																Color(0xff14279B),
																Color(0xff14279B),
															],
														),
													),
													child: Text(
														'Login',
														style: TextStyle(fontSize: 20, color: Colors.white),
													),
												),
											),
											SizedBox(height: height * .055),
										],
									),
								),
							),
							Positioned(
								top: 40,
								left: 0,
								child: InkWell(
									onTap: () {
										Navigator.pop(context);
									},
									child: Container(
										padding: EdgeInsets.symmetric(horizontal: 10),
										child: Row(
											children: <Widget>[
												Container(
													padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
													child:
													Icon(Icons.keyboard_arrow_left, color: Colors.black),
												),
												Text('Back',
														style: TextStyle(
																fontSize: 12, fontWeight: FontWeight.w500))
											],
										),
									),
								),
							),
						],
					),
				));
	}
}

void toast(String msg){
	Fluttertoast.showToast(
			msg: msg,
			toastLength: Toast.LENGTH_SHORT,
			gravity: ToastGravity.BOTTOM,
			timeInSecForIosWeb: 1,
			backgroundColor: Colors.black54,
			textColor: Colors.white,
			fontSize: 16.0
	);
}