import 'dart:convert';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:login/home_screen.dart';
import 'package:login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SignUpScreen extends StatefulWidget {
	const SignUpScreen({Key? key}) : super(key: key);

	@override
	_SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {

	TextEditingController firstNameController = new TextEditingController();
	TextEditingController lastNameController = new TextEditingController();
	TextEditingController emailController = new TextEditingController();
	TextEditingController passwordController = new TextEditingController();
	TextEditingController passwordConfirmController = new TextEditingController();

	@override
	Widget build(BuildContext context) {
		final height = MediaQuery.of(context).size.height;
		return Scaffold(
			body: Container(
				height: height,
				child: Stack(
					children: <Widget>[
						Positioned(
							child: Container(
									child: ClipPath(
										child: Container(
											decoration: BoxDecoration(
												gradient: LinearGradient(
													begin: Alignment.topCenter,
													end: Alignment.bottomCenter,
													colors: [
														Color(0xffE6E6E6),
														Color(0xff14279B),
													],
												),
											),
										),
									)),
						),
						Container(
							padding: EdgeInsets.symmetric(horizontal: 40),
							child: SingleChildScrollView(
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.center,
									mainAxisAlignment: MainAxisAlignment.center,
									children: <Widget>[
										SizedBox(height: height * .2),
										Column(
											children: <Widget>[
												Container(
													margin: EdgeInsets.symmetric(vertical: 10),
													child: Column(
														crossAxisAlignment: CrossAxisAlignment.start,
														children: <Widget>[
															Text(
																"First Name",
																style: TextStyle(
																		fontWeight: FontWeight.bold, fontSize: 15),
															),
															SizedBox(
																height: 10,
															),
															TextField(
																	controller: firstNameController,
																	obscureText: false,
																	decoration: InputDecoration(
																			border: InputBorder.none,
																			fillColor: Color(0xfff3f3f4),
																			filled: true))
														],
													),
												),
												Container(
													margin: EdgeInsets.symmetric(vertical: 10),
													child: Column(
														crossAxisAlignment: CrossAxisAlignment.start,
														children: <Widget>[
															Text(
																"Last Name",
																style: TextStyle(
																		fontWeight: FontWeight.bold, fontSize: 15),
															),
															SizedBox(
																height: 10,
															),
															TextField(
																	controller: lastNameController,
																	obscureText: true,
																	decoration: InputDecoration(
																			border: InputBorder.none,
																			fillColor: Color(0xfff3f3f4),
																			filled: true))
														],
													),
												),
												Container(
													margin: EdgeInsets.symmetric(vertical: 10),
													child: Column(
														crossAxisAlignment: CrossAxisAlignment.start,
														children: <Widget>[
															Text(
																"Email",
																style: TextStyle(
																		fontWeight: FontWeight.bold, fontSize: 15),
															),
															SizedBox(
																height: 10,
															),
															TextField(
																	controller: emailController,
																	obscureText: false,
																	decoration: InputDecoration(
																			border: InputBorder.none,
																			fillColor: Color(0xfff3f3f4),
																			filled: true))
														],
													),
												),
												Container(
													margin: EdgeInsets.symmetric(vertical: 10),
													child: Column(
														crossAxisAlignment: CrossAxisAlignment.start,
														children: <Widget>[
															Text(
																"Password",
																style: TextStyle(
																		fontWeight: FontWeight.bold, fontSize: 15),
															),
															SizedBox(
																height: 10,
															),
															TextField(
																	controller: passwordController,
																	obscureText: true,
																	decoration: InputDecoration(
																			border: InputBorder.none,
																			fillColor: Color(0xfff3f3f4),
																			filled: true))
														],
													),
												),
												Container(
													margin: EdgeInsets.symmetric(vertical: 10),
													child: Column(
														crossAxisAlignment: CrossAxisAlignment.start,
														children: <Widget>[
															Text(
																"Password Confirm",
																style: TextStyle(
																		fontWeight: FontWeight.bold, fontSize: 15),
															),
															SizedBox(
																height: 10,
															),
															TextField(
																	controller: passwordConfirmController,
																	obscureText: true,
																	decoration: InputDecoration(
																			border: InputBorder.none,
																			fillColor: Color(0xfff3f3f4),
																			filled: true))
														],
													),
												),
											],
										),
										SizedBox(
											height: 20,
										),
										GestureDetector(
											onTap: () {
												if (
												firstNameController.text.isEmpty ||
														lastNameController.text.isEmpty ||
														emailController.text.isEmpty ||
														passwordController.text.isEmpty ||
														passwordConfirmController.text.isEmpty
												) {
													toast("FirstName or LastName or Email or Password or PasswordConfirm is Empty!");
													return;
												}


												// Check password and password confirm
												if (passwordController.text != passwordConfirmController.text) {
													toast("密碼不相符");
													return;
												}

												// Check Password
												String pattern = r'^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,}$';
												RegExp regExp = new RegExp(pattern);
												if (!regExp.hasMatch(passwordController.text)) {
													toast("密碼最少8個字元並且至少一個英文跟一個數字組合");
													return;
												}

												Dio().post('http://10.0.2.2:3000/api/register',
														data: {
															'firstname': firstNameController.text,
															'lastname': lastNameController.text,
															'email': emailController.text,
															'password': passwordController.text,
															'password2': passwordConfirmController.text
														})
														.then((value) {
													print(value);
													try {
														Map<String, dynamic> data = json.decode(value.toString());
														toast(data["message"]);
														if (data["message"] == 'user created') {
															Navigator.pushReplacement(
																context,
																MaterialPageRoute(
																	builder: (context) => HomeScreen(),
																),
															);
														}
													} catch (e) {
														toast("ERROR");
													}
												}).onError((error, stackTrace) {
													print(error);
													toast("ERROR");
												});
											},
//											onTap: () => Navigator.pushReplacement(
//												context,
//												MaterialPageRoute(
//													builder: (context) => HomeScreen(),
//												),
//											),
											child: Container(
												width: MediaQuery.of(context).size.width,
												padding: EdgeInsets.symmetric(vertical: 15),
												alignment: Alignment.center,
												decoration: BoxDecoration(
													borderRadius: BorderRadius.all(Radius.circular(5)),
													boxShadow: <BoxShadow>[
														BoxShadow(
																color: Colors.grey.shade200,
																offset: Offset(1, 2),
																blurRadius: 5,
																spreadRadius: 1)
													],
													gradient: LinearGradient(
														begin: Alignment.centerLeft,
														end: Alignment.centerRight,
														colors: [
															Color(0xff14279B),
															Color(0xff14279B),
														],
													),
												),
												child: Text(
													'Register Now',
													style: TextStyle(fontSize: 20, color: Colors.white),
												),
											),
										),
//										SizedBox(height: height * .14),
//										InkWell(
//											onTap: () {
//												Navigator.push(
//														context,
//														MaterialPageRoute(
//																builder: (context) => LoginScreen()));
//											},
//											child: Container(
//												margin: EdgeInsets.symmetric(vertical: 20),
//												padding: EdgeInsets.all(15),
//												alignment: Alignment.bottomCenter,
//												child: Row(
//													mainAxisAlignment: MainAxisAlignment.center,
//													children: <Widget>[
//														Text(
//															'Already have an account ?',
//															style: TextStyle(
//																	fontSize: 13, fontWeight: FontWeight.w600),
//														),
//														SizedBox(
//															width: 10,
//														),
//														Text(
//															'Login',
//															style: TextStyle(
//																	color: Color(0xff14279B),
//																	fontSize: 13,
//																	fontWeight: FontWeight.w600),
//														),
//													],
//												),
//											),
//										),
									],
								),
							),
						),
						Positioned(
							top: 40,
							left: 0,
							child: InkWell(
								onTap: () {
									Navigator.pop(context);
								},
								child: Container(
									padding: EdgeInsets.symmetric(horizontal: 10),
									child: Row(
										children: <Widget>[
											Container(
												padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
												child: Icon(Icons.keyboard_arrow_left,
														color: Colors.black),
											),
											Text('Back',
													style: TextStyle(
															fontSize: 12, fontWeight: FontWeight.w500))
										],
									),
								),
							),
						),
					],
				),
			),
		);
	}
}


void toast(String msg){
	Fluttertoast.showToast(
			msg: msg,
			toastLength: Toast.LENGTH_SHORT,
			gravity: ToastGravity.BOTTOM,
			timeInSecForIosWeb: 1,
			backgroundColor: Colors.black54,
			textColor: Colors.white,
			fontSize: 16.0
	);
}